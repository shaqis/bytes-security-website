<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/multipurpose_corporate_theme/templates/page.html.twig */
class __TwigTemplate_1284b13aa8d3cd5aafb90610e0fcf0e2a8a5954eadabd23aaf567a3c6da3a976 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 12];
        $filters = ["escape" => 7, "t" => 15];
        $functions = ["attach_library" => 8];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape', 't'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attach_library"] ?? null)), "html", null, true);
        echo "
";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("multipurpose_corporate_theme/multipurpose_corporate_theme.layout.page"), "html", null, true);
        echo "

<div id=\"page-wrapper\">
    <div id=\"page\" class=\"container\">
        ";
        // line 12
        if ($this->getAttribute(($context["page"] ?? null), "leaderboard", [])) {
            // line 13
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "leaderboard", [])), "html", null, true);
            echo "
        ";
        }
        // line 15
        echo "        <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Site header"));
        echo " \">
            <div class=\"section layout-container clearfix\">
                ";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
            </div>
        </header>
        ";
        // line 20
        if ($this->getAttribute(($context["page"] ?? null), "menu_bar", [])) {
            // line 21
            echo "            <div class=\"menu_bar\">
                <div class=\"layout-container section clearfix\" role=\"complementary\">
                    ";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "menu_bar", [])), "html", null, true);
            echo "
                </div>
            </div>
        ";
        }
        // line 27
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "secondary_content", [])) {
            // line 28
            echo "            <div class=\"region-secondary-content\">
                <div class=\"region-inner clearfix\">
                    <div class=\"section\" role=\"complementary\">
                        ";
            // line 31
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "secondary_content", [])), "html", null, true);
            echo "
                    </div>
                </div>
            </div>
        ";
        }
        // line 36
        echo "        <div id=\"columns\" class=\"columns clearfix\">
            <main id=\"content-column\" class=\"content-column\" role=\"main\">
                <div class=\"content-inner\">
                    ";
        // line 39
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 40
            echo "                        <div class=\"highlighted\">
                            <div class=\"layout-container section clearfix\" role=\"complementary\">
                                ";
            // line 42
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
                            </div>
                        </div>
                    ";
        }
        // line 46
        echo "                    <section id=\"main-content\" class=\"contextual-links-region\">
                        <div id=\"content\" class=\"region\">
                            ";
        // line 48
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
                        </div>
                    </section>
                </div>
            </main>
            ";
        // line 53
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 54
            echo "                <div class=\"region-sidebar-second sidebar\">
                    <div class=\"region-inner clearfix\">
                        <aside class=\"section\" role=\"complementary\">
                            ";
            // line 57
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
                        </aside>
                    </div>
                </div>
            ";
        }
        // line 62
        echo "        </div>
    </div>
    <div class=\"footer-wrapper\">
        ";
        // line 65
        if ($this->getAttribute(($context["page"] ?? null), "tertiary", [])) {
            // line 66
            echo "            <div class=\"region region-tertiary-content\">
                <div class=\"region-inner clearfix\">
                    ";
            // line 68
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "tertiary", [])), "html", null, true);
            echo "
                </div>
            </div>
        ";
        }
        // line 72
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 73
            echo "        <footer class=\"site-footer\">
            <div class=\"region region-footer\">
                <div class=\"region-inner clearfix\">
                    ";
            // line 76
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
                </div>
            </div>
        </footer>
        ";
        }
        // line 81
        echo "    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "themes/multipurpose_corporate_theme/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 81,  186 => 76,  181 => 73,  178 => 72,  171 => 68,  167 => 66,  165 => 65,  160 => 62,  152 => 57,  147 => 54,  145 => 53,  137 => 48,  133 => 46,  126 => 42,  122 => 40,  120 => 39,  115 => 36,  107 => 31,  102 => 28,  99 => 27,  92 => 23,  88 => 21,  86 => 20,  80 => 17,  74 => 15,  68 => 13,  66 => 12,  59 => 8,  55 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/multipurpose_corporate_theme/templates/page.html.twig", "/Applications/MAMP/htdocs/bytesdev/themes/multipurpose_corporate_theme/templates/page.html.twig");
    }
}
