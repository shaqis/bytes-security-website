<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/multipurpose_corporate_theme/templates/html.html.twig */
class __TwigTemplate_ec4145f226cdaac536196c333b0ca96d23ee1f28e6812b989c33c4120cf41703 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 42, "if" => 49];
        $filters = ["escape" => 39, "raw" => 66, "safe_join" => 67, "clean_class" => 74, "t" => 86];
        $functions = ["attach_library" => 39];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape', 'raw', 'safe_join', 'clean_class', 't'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary(($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["theme"] ?? null), "name", [])) . "/global-styling")), "html", null, true);
        echo "
<!DOCTYPE html>
";
        // line 42
        $context["html_classes"] = [0 => "no-js", 1 => "adaptivetheme"];
        // line 47
        echo "<html";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["html_attributes"] ?? null), "addClass", [0 => ($context["html_classes"] ?? null)], "method")), "html", null, true);
        echo ">
  <head>
    ";
        // line 49
        if ((($context["touch_icons"] ?? null) == true)) {
            // line 50
            echo "      ";
            if (($context["touch_icon_ipad_retina"] ?? null)) {
                // line 51
                echo "        <link href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_icon_ipad_retina"] ?? null)), "html", null, true);
                echo "\" rel=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_rel"] ?? null)), "html", null, true);
                echo "\" sizes=\"152x152\" />
      ";
            }
            // line 53
            echo "      ";
            if (($context["touch_icon_iphone_retina"] ?? null)) {
                // line 54
                echo "        <link href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_icon_iphone_retina"] ?? null)), "html", null, true);
                echo "\" rel=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_rel"] ?? null)), "html", null, true);
                echo "\" sizes=\"120x120\" />
      ";
            }
            // line 56
            echo "      ";
            if (($context["touch_icon_ipad"] ?? null)) {
                // line 57
                echo "        <link href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_icon_ipad"] ?? null)), "html", null, true);
                echo "\" rel=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_rel"] ?? null)), "html", null, true);
                echo "\" sizes=\"76x76\" />
      ";
            }
            // line 59
            echo "      ";
            if (($context["touch_icon_default"] ?? null)) {
                // line 60
                echo "        <link href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_icon_default"] ?? null)), "html", null, true);
                echo "\" rel=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_rel"] ?? null)), "html", null, true);
                echo "\" sizes=\"60x60\" />
      ";
            }
            // line 62
            echo "      ";
            if (($context["touch_icon_nokia"] ?? null)) {
                // line 63
                echo "        <link href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["touch_icon_nokia"] ?? null)), "html", null, true);
                echo "\" rel=\"shortcut icon\" />
      ";
            }
            // line 65
            echo "    ";
        }
        // line 66
        echo "    <head-placeholder token=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
    <title>";
        // line 67
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->safeJoin($this->env, $this->sandbox->ensureToStringAllowed(($context["head_title"] ?? null)), " | "));
        echo "</title>
    <css-placeholder token=\"";
        // line 68
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
    <js-placeholder token=\"";
        // line 69
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
  </head>
  ";
        // line 72
        $context["body_classes"] = [0 => ((        // line 73
($context["logged_in"] ?? null)) ? ("user-logged-in") : ("")), 1 => (( !        // line 74
($context["root_path"] ?? null)) ? ("path-frontpage") : (("path-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["root_path"] ?? null)))))), 2 => (($this->getAttribute(        // line 75
($context["path_info"] ?? null), "args", [])) ? (("path-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["path_info"] ?? null), "args", [])))) : ("")), 3 => (($this->getAttribute(        // line 76
($context["path_info"] ?? null), "query", [])) ? (("path-query-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["path_info"] ?? null), "query", [])))) : ("")), 4 => ((        // line 77
($context["node_type"] ?? null)) ? (("node--type-" . $this->sandbox->ensureToStringAllowed(($context["node_type"] ?? null)))) : ("")), 5 => (($this->getAttribute(        // line 78
($context["url"] ?? null), "path", [])) ? (("page--url-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), "path", [])))) : ("")), 6 => (($this->getAttribute(        // line 79
($context["head_title_array"] ?? null), "name", [])) ? (("site-name--" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["head_title_array"] ?? null), "name", []))))) : ("")), 7 => (($this->getAttribute(        // line 80
($context["theme"] ?? null), "name", [])) ? (("theme-name--" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["theme"] ?? null), "name", []))))) : ("")), 8 => ((        // line 81
($context["db_offline"] ?? null)) ? ("db-offline") : ("")), 9 => (($this->getAttribute(        // line 82
($context["page"] ?? null), "sidebar_second", [])) ? ("one-sidebar sidebar-second") : (""))];
        // line 85
        echo "  <body ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["body_classes"] ?? null)], "method")), "html", null, true);
        echo ">
    <a href=\"";
        // line 86
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["skip_link_target"] ?? null)), "html", null, true);
        echo "\" class=\"visually-hidden focusable skip-link\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Skip to main content"));
        echo "</a>
    ";
        // line 87
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_top"] ?? null)), "html", null, true);
        echo "
    ";
        // line 88
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page"] ?? null)), "html", null, true);
        echo "
    ";
        // line 89
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_bottom"] ?? null)), "html", null, true);
        echo "
    <js-bottom-placeholder token=\"";
        // line 90
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
  </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "themes/multipurpose_corporate_theme/templates/html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 90,  175 => 89,  171 => 88,  167 => 87,  161 => 86,  156 => 85,  154 => 82,  153 => 81,  152 => 80,  151 => 79,  150 => 78,  149 => 77,  148 => 76,  147 => 75,  146 => 74,  145 => 73,  144 => 72,  139 => 69,  135 => 68,  131 => 67,  126 => 66,  123 => 65,  117 => 63,  114 => 62,  106 => 60,  103 => 59,  95 => 57,  92 => 56,  84 => 54,  81 => 53,  73 => 51,  70 => 50,  68 => 49,  62 => 47,  60 => 42,  55 => 39,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/multipurpose_corporate_theme/templates/html.html.twig", "/Applications/MAMP/htdocs/bytesdev/themes/multipurpose_corporate_theme/templates/html.html.twig");
    }
}
